*** Settings ***
Library           OperatingSystem
Library           String
Library           Process
Library           BuiltIn
Suite Teardown    Teardown Actions

*** Variables ***
${testPlan}       No test plan
${actionType}     GLOBAL LOAD
${latestRun}      false
${splitString}    .
${cleanDir}    true
${callsGT}    0
${callsLT}    sys.maxint
${minGT}    0
${minLT}     sys.maxint
${maxGT}    0
${maxLT}     sys.maxint
${meanGT}    0
${meanLT}     sys.maxint
${medianGT}    0
${medianLT}     sys.maxint
${stdDevGT}    0
${stdDevLT}     sys.maxint
${throughputGT}    0
${throughputLT}     sys.maxint
${errorsGT}    0
${errorsLT}     sys.maxint
*** Test Cases ***

CallsTest
    Run Keyword if    """${splitString}"""=="."    RetreiveTest   
    ${callsVal}=    Get Line    ${splitString}[1]    0
    ${callsVal}=    Split String    ${callsVal}    ,
    ${callsVal}=    Get Line    ${callsVal}[0]    0
    ${callsVal}=    Convert To Integer    ${callsVal}
    Should Be True    ${callsVal}>=${callsGT}
    Should Be True    ${callsVal}<=${callsLT}

MinTest
    Run Keyword if    """${splitString}"""=="."    RetreiveTest   
    ${minVal}=    Get Line    ${splitString}[ 2]    0
    ${minVal}=    Split String    ${minVal}    ,
    ${minVal}=    Get Line    ${minVal}[0]    0
    ${minVal}=    Convert To Integer    ${minVal}
    Should Be True    ${minVal}>=${minGT}
    Should Be True    ${minVal}<=${minLT}

MaxTest
    Run Keyword if    """${splitString}"""=="."    RetreiveTest   
    ${maxVal}=    Get Line    ${splitString}[ 3]    0
    ${maxVal}=    Split String    ${maxVal}    ,
    ${maxVal}=    Get Line    ${maxVal}[0]    0
    ${maxVal}=    Convert To Integer    ${maxVal}
    Should Be True    ${maxVal}>=${maxGT}
    Should Be True    ${maxVal}<=${maxLT}

MeanTest
    Run Keyword if    """${splitString}"""=="."    RetreiveTest   
    ${meanVal}=    Get Line    ${splitString}[4]    0
    ${mean}=    Split String    ${meanVal}    ,
    ${mean}=    Get Line    ${mean}[0]    0
    ${mean}=    Convert To Integer    ${mean}
    Should Be True    ${mean}>=${meanGT}
    Should Be True    ${mean}<=${meanLT}

MedianTest
    Run Keyword if    """${splitString}"""=="."    RetreiveTest   
    ${medianVal}=    Get Line    ${splitString}[5]    0
    ${median}=    Split String    ${medianVal}    ,
    ${median}=    Get Line    ${median}[0]    0
    ${median}=    Convert To Integer    ${median}
    Should Be True    ${median}>=${medianGT}
    Should Be True    ${median}<=${medianLT}

StdDevTest
    Run Keyword if    """${splitString}"""=="."    RetreiveTest   
    ${std devVal}=    Get Line    ${splitString}[ 6]    0
    ${std dev}=    Split String    ${std devVal}    ,
    ${std dev}=    Get Line    ${std dev}[0]    0
    ${std dev}=    Convert To Integer    ${std dev}
    Should Be True    ${std dev}>=${stdDevGT}
    Should Be True    ${std dev}<=${stdDevLT}

ThroughputTest
    Run Keyword if    """${splitString}"""=="."    RetreiveTest   
    ${throughputVal}=    Get Line    ${splitString}[ 7]    0
    ${throughput}=    Split String    ${throughputVal}    ,
    ${throughput}=    Get Line    ${throughput}[0]    0
    ${throughput}=    Convert To Integer    ${throughput}
    Should Be True    ${throughput}>=${throughputGT}
    Should Be True    ${throughput}<=${throughputLT}

ErrorsTest
    Run Keyword if    """${splitString}"""=="."    RetreiveTest   
    ${errorsVal}=    Get Line    ${splitString}[ 8]    0
    ${errors}=    Split String    ${errorsVal}    ,
    ${errors}=    Get Line    ${errors}[0]    0
    ${errors}=    Convert To Integer    ${errors}
    Should Be True    ${errors}>=${errorsGT}
    Should Be True    ${errors}<=${errorsLT}

*** Keywords ***
RetreiveTest
    Run keyword Unless    "true" in """${latestRun} """    FirstRetreiveTest
    Run keyword If    "true" in """${latestRun} """    SubsequentialRetreiveTest

FirstRetreiveTest
    ClifLaunchTest
    ${clifRetreive}=    Run    clifcmd quickstats
    WriteVariableInFile    ${clifRetreive}
    ${lineToTest}=    Get Lines Containing String    ${clifRetreive}    ${actionType}
    ${splitString}=    Split String    ${lineToTest}    \t
    Set Global Variable    ${splitString}

SubsequentialRetreiveTest
    ${clifRetreive}=    Get File    ${EXECDIR}/LastTestResult.txt
    ${lineToTest}=    Get Lines Containing String    ${clifRetreive}    ${actionType}
    ${splitString}=    Split String    ${lineToTest}    \t
    Set Global Variable    ${splitString}

WriteVariableInFile
    [Arguments]    ${clifRetreive}
    Create File    ${EXECDIR}/LastTestResult.txt    ${clifRetreive}

ClifLaunchTest
    ${filePath}=    Split String From Right    ${testPlan}    /
    ${lengthfilePath}=    Get Length    ${filePath}
    Run keyword Unless    ${LengthfilePath}==1    ClifLaunchFromDir    ${filePath}
    Run keyword If    ${LengthfilePath}==1    ClifLaunchCurrentDir    ${filePath}

ClifLaunchFromDir
    [Arguments]    ${filePath}
    ${filename}=    Split String    ${filePath}[1]     .
    ${clifLaunch}=    Run    JAVA_OPTS="-Dclif.filestorage.clean=auto -Dclif.codeserver.path=${filePath}[0]/" clifcmd launch test ${testPlan} ${filename}[0]
    Should Be True    "Initialized" in """${clifLaunch}"""
    
ClifLaunchCurrentDir
    [Arguments]    ${filePath}
    ${filename}=    Split String    ${filePath}[0]     .
    ${clifLaunch}=    Run    clifcmd launch test ${testPlan} ${filename}[0]
    Should Be True    "Initialized" in """${clifLaunch}"""
    
Teardown Actions
    Run Keyword If   "true" in """${cleanDir}"""    CleanDir   
    
    
CleanDir
    Empty Directory    report
    Remove Directory    report
    Remove Files    LastTestResult.txt
    
