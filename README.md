<h1>CLIF integration to Robot Framework</h1>

![paclif_100](https://clif.ow2.io/images/logo_clif_200px.gif)


The ClifIntegration.robot file can be used to test metrics returned by CLIF test with Robot Framework.
It can be launched with Ride, Eclipse or by command line.

<h2>Variables</h2>

Most variables are editable by executing ClifIntegration.robot the following way:

	robot -v myVariable1:myValue1 -v myVariable2:myValue2 ClifIntegration.robot

Or by adding *-v myVariable1:myValue1* to Ride or Eclipse run configuration.

* *testPlan* is the path to your clif plan file, and is mandatory.
* *actionType* precises which action type within your test plan will be tested. If no value is given, robot will test metrics corresponding to the global load.
* *latestRun* can be set to true if you want to launch robot using the same test plan as your latest run and do not want to wait for another execution of clif.

* *callsGT, callsLT, minGT, minLT, maxGT, maxLT, meanGT, meanLT, medianGT, medianLT, stdDevGT, stdDevLT, throughputGT, throughputLT, errorsGT, errorsLT* allow you to set minimum or maximum values for each given metric so that the robot test will pass or fail if the metric's value is between metricGT and metricLT.  Base values are 0 for metricGT and int.maxsize for metricLT. More info about metrics below.

<h2> Tests and Metrics </h2>

The following metrics are returned by clif and can be tested with robot:
* Calls : number of successful requests
* Min : minimum response times 
* Max : maximum response times 
* Mean : mean response times 
* Median : median response times 
* Std Dev: standard deviation of the set of response times
* Throughput: throughput
* Errors: number of unsuccessful requests

By default, this program will test all metrics with their respective GT and LT values.
However, if the user wants to test only some metrics, he can do so by precising which test to execute, either by using Eclipse or Ride interface or by adding -t MetricTest in the command line like below:

    robot -t MeanTest -v meanGT:500 -v testPlan:example.ctp ClifIntegration.robot

<h2> Installation </h2>

Both Clif and Robot Framework environment variable must exist in your PATH to use this script.
To install clifcmd, download the latest clif-server from https://release.ow2.org/clif/ then extract here somewhere and add the path to the bin/ dir to your PATH variable.
